﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SiteInputTestAsync
{
    public class DisplayItem
    {
        public string PropertyName { get; private set; }
        public string PropertyDisplayName { get; private set; }
        public string PropertyValue { get; set; }
        public string PropertyMeasurement { get; set; }
        public string Group { get; private set; }
        public bool Show { get; set; }

        [JsonConstructor]
        public DisplayItem(string propertyName, string propertyDisplayName, string propertyValue, string propertyMeasurement, string group) 
            : this(propertyName, propertyDisplayName, propertyValue, propertyMeasurement, group, true) { }

        public DisplayItem(string propertyName, string propertyDisplayName, string propertyValue, string propertyMeasurement)
            : this(propertyName, propertyDisplayName, propertyValue, propertyMeasurement, "None", true) { }

        public DisplayItem(string propertyName, string propertyDisplayName, string propertyValue, string propertyMeasurement, string group, bool show) { 
            this.PropertyName = propertyName;
            this.PropertyDisplayName = propertyDisplayName;
            this.PropertyValue = propertyValue;
            this.PropertyMeasurement = propertyMeasurement;
            this.Group = group;
            this.Show = show; //Property is shown by default unless it's flagged not to be.
        }
        public DisplayItem(DisplayItem originalItem) {
            this.PropertyName = originalItem.PropertyName;
            this.PropertyDisplayName = originalItem.PropertyDisplayName;
            this.PropertyValue = originalItem.PropertyValue;
            this.PropertyMeasurement = originalItem.PropertyMeasurement;
            this.Group = originalItem.Group;
            this.Show = originalItem.Show; //Pro
        }
        public DisplayItem() { }
    }
}