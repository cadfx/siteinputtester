﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace SiteInputTestAsync
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static bool log = false;
        static string host = ConfigurationManager.AppSettings["BaseDomain"];
        static string testFile = "Example Inputs.csv";
        static void Main(string[] args)
        {
            log = (ConfigurationManager.AppSettings["Log"] == "1");
            if (args.Length > 0)
            {
                testFile = args[0];
            }
            RunAsync().Wait();
        }
        static async Task RunAsync()
        {
            using (StreamWriter w = File.AppendText(ConfigurationManager.AppSettings["LogFile"]))
            {
                try
                {
                    LogDateTime(w);
                    // Create a new request

                    JObject allModelData = LoadCSV(testFile);
                    LogLine(string.Format("Host: {0}", host), w);
                    client.BaseAddress = new Uri(host);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    foreach (KeyValuePair<string, JToken> kvp in allModelData)
                    {
                        JToken requestData = allModelData[kvp.Key];
                        int testInstalledStorageVolume = Convert.ToInt32(requestData["InstalledStorageVolume"]);
                        double testStorageVolumePerChamber = Convert.ToDouble(requestData["StorageVolumePerChamber"]);
                        int testApproxBedSizeRequired = Convert.ToInt32(requestData["ApproxBedSizeRequired"]);
                        int testNumberOfChambersRequired = Convert.ToInt32(requestData["NumberOfChambersRequired"]);

                        var responseData = await MakeRequestAsync(requestData);
                        JArray resultOutputs = GetResultOutputsFromAPIResponse(responseData);
                        List<DisplayItem> itemList = GetDisplayItemListFromResultOutputs(resultOutputs);

                        int actualInstalledStorageVolume = GetIntFromDisplayItemList(itemList, "InstalledStorageVolume");
                        double actualStorageVolumePerChamber = GetDoubleFromDisplayItemList(itemList, "StorageVolumePerChamber");
                        int actualApproxBedSizeRequired = GetIntFromDisplayItemList(itemList, "BedSize");
                        int actualNumberOfChambersRequired = GetIntFromDisplayItemList(itemList, "NumberOfChambersRequired");

                        if (actualInstalledStorageVolume != testInstalledStorageVolume ||
                            actualStorageVolumePerChamber != testStorageVolumePerChamber ||
                            actualApproxBedSizeRequired != testApproxBedSizeRequired ||
                            actualNumberOfChambersRequired != testNumberOfChambersRequired)
                        {
                            string notExpectedMessage = String.Format("Result is bad!: \"{0}\" did not retrieve the expected values.\n", kvp.Key);
                            notExpectedMessage += String.Format("Key:\t\t\t\tExpected Result:\tActualResult:\n");
                            notExpectedMessage += String.Format("InstalledStorageVolume\t\t{0}\t\t\t{1}\n", testInstalledStorageVolume, actualInstalledStorageVolume);
                            notExpectedMessage += String.Format("StorageVolumePerChamber\t\t{0}\t\t\t{1}\n", testStorageVolumePerChamber, actualStorageVolumePerChamber);
                            notExpectedMessage += String.Format("ApproxBedSizeRequired\t\t{0}\t\t\t{1}\n", testApproxBedSizeRequired, actualApproxBedSizeRequired);
                            notExpectedMessage += String.Format("NumberOfChambersRequired\t{0}\t\t\t{1}\n", testNumberOfChambersRequired, actualNumberOfChambersRequired);
                            LogLine(notExpectedMessage, w);
                            //throw new Exception(notExpectedMessage);
                        }
                        else
                        {
                            string expectedMessage = String.Format("Result is good: \"{0}\" retrieved the expected values.", kvp.Key);
                            LogLine(expectedMessage, w);
                        }

                    }
                }
                catch (Exception e)
                {
                    LogLine(e.Message, w);
                    if(ConfigurationManager.AppSettings["Email"] == "1")
                    {
                        ErrorEmailMessage errorEmail = new ErrorEmailMessage(e);
                        errorEmail.SendMailMessage("dlh@cadfx.com", new List<string>() { "dkurin@swiftlet.technology" });
                    }
                }
                finally
                {
                    
                }
            }

            Console.WriteLine();
            Console.Write("Test complete, press Enter to close.");
            Console.ReadLine();
        }

        public static void LogLine(string logMessage, TextWriter w)
        {
            Console.WriteLine(logMessage);
            if (log)
            {
                w.WriteLine("{0}", logMessage);
            }
        }
        public static void LogDateTime(TextWriter w)
        {
            if (log)
            {
                w.Write("\r\nLog Entry : ");
                w.WriteLine("{0} {1}:", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                //w.WriteLine("  :");
            }
        }
        private static int GetIntFromDisplayItemList(List<DisplayItem> itemList, string key)
        {
            return Convert.ToInt32(itemList.Where(displayItem => displayItem.PropertyName == key).First().PropertyValue);
        }
        private static double GetDoubleFromDisplayItemList(List<DisplayItem> itemList, string key)
        {
            return Convert.ToDouble(itemList.Where(displayItem => displayItem.PropertyName == key).First().PropertyValue);
        }
        private static List<DisplayItem> GetDisplayItemListFromResultOutputs(JArray resultOutputs)
        {
            return resultOutputs.ToObject<List<DisplayItem>>();
        }
        private static JArray GetResultOutputsFromAPIResponse(object responseData)
        {
            JObject responseObject = JObject.Parse(responseData.ToString());
            JArray resultOutputs = (JArray)responseObject["ResultOutputs"];
            return resultOutputs;
        }
        static async Task<object> MakeRequestAsync(JToken product)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync("api/results", product);
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            string responseBody = await response.Content.ReadAsStringAsync();
            return responseBody;
        }
        static JObject LoadJson()
        {
            using (StreamReader r = new StreamReader("ModelInputs.json"))
            {
                string json = r.ReadToEnd();
                JObject jsonObject = JObject.Parse(json);
                return jsonObject;
            }
        }
        static JObject LoadCSV(string filePath)
        {
            Console.WriteLine(filePath);
            using (StreamReader r = new StreamReader(File.OpenRead(filePath)))
            {
                // Read all data out of CSV
                string line = r.ReadLine();
                int lineNo = 0;
                bool dataSection = false;
                List<string> names = new List<string>();
                JObject jsonObject = new JObject();
                bool skip = false;
                while (!r.EndOfStream)
                {
                    line = r.ReadLine();
                    lineNo++;
                    string[] l = line.Split(',');
                    if (l[0] == "Host") host = l[1];

                    if (l[0] == "Input Name")
                    {
                        foreach(string name in l)
                        {
                            names.Add(name);
                        }
                        skip = true;
                    }

                    if (!skip && names.Count > 2)
                    {
                        JObject input = new JObject();
                        for( int i = 1; i < names.Count; i++ )
                        {
                            input[names[i]] = l[i];
                        }
                        jsonObject.Add(l[0], input);
                    }

                    skip = false;
                }

                // Format it into JSON
                string json = r.ReadToEnd();
                //jsonObject = JObject.Parse(json);
                return jsonObject;
            }
        }
    }
}
