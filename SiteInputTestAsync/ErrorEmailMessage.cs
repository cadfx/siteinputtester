﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using System.Net.Mail;
using System.Configuration;

namespace SiteInputTestAsync
{
    class ErrorEmailMessage : SendGridEmailMessage
    {
        public Exception Exception { get; set; }
        public ErrorEmailMessage(Exception exception)
        {
            this.Exception = exception;
        }
        public override SendGridMessage ComposeMailMessage(string sendFrom, List<string> sendTo)
        {
            SendGridMessage myMessage = new SendGridMessage();
            myMessage.From = new MailAddress(sendFrom);
            myMessage.AddTo(sendTo);
            myMessage.Subject = ConfigurationManager.AppSettings["ExceptionEmailSubject"];
            myMessage.Html = string.Format("<p>There was an Exception caught from the StormTech AutoRun testing app: <br><br>{0}<br><br>Stack Trace: <br>{1}<br><br>You may want to check the site.</p>", this.Exception.Message, this.Exception.StackTrace);
            return myMessage;
        }
    }
}
