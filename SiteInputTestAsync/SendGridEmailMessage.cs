﻿using SendGrid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace SiteInputTestAsync
{
    public abstract class SendGridEmailMessage
    {
        public void SendMailMessage(SendGridMessage newMessage)
        {
            // Create credentials, specifying your user name and password.
            var credentials = new NetworkCredential(
                ConfigurationManager.AppSettings["api_user"],
                ConfigurationManager.AppSettings["api_key"]
                );
            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            transportWeb.Deliver(newMessage);
        }
        public abstract SendGridMessage ComposeMailMessage(string sendFrom, List<String> sendTo);


        public void SendMailMessage(string sendFrom, List<String> sendTo)
        {
            SendGridMessage newMessage = ComposeMailMessage(sendFrom, sendTo);
            SendMailMessage(newMessage);
        }
    }
}